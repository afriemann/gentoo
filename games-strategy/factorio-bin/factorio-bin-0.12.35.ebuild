# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils

DESCRIPTION="Factorio is a 2D game about building factories on an alien planet."
HOMEPAGE="https://www.factorio.com/"
LICENSE="GPL"

KEYWORDS="-* amd64 -x86"

SLOT=0

DEPEND=""

RESTRICT="fetch"

SRC_URI="${PN/-bin/}_alpha_x64_${PV}.tar.gz"

S="${WORKDIR}/factorio"

pkg_nofetch() {
	einfo "Please download"
	einfo "  - ${SRC_URI}"
	einfo "from ${HOMEPAGE} and place them in ${DISTDIR}"
	einfo "alternatively install fctdl via pip and run:"
	einfo "  $ fctdl ${PV} ${DISTDIR}"
}

src_install() {
	dodir /usr/share/factorio
	cp -R "${S}"/* "${D}"/usr/share/factorio/ || die "Install failed!"
	mv "${D}"/usr/share/factorio/data/* "${D}"/usr/share/factorio || die "Install failed!"
	rm -r "${D}"/usr/share/factorio/data

	dosym /usr/share/factorio/bin/x64/factorio /usr/local/bin/factorio
}
