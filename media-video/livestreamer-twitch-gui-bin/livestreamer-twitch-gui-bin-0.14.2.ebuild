# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

inherit eutils pax-utils

DESCRIPTION="A multi platform Twitch.tv browser for Livestreamer"
HOMEPAGE="https://github.com/bastimeyer/livestreamer-twitch-gui"
SRC_URI="https://github.com/bastimeyer/livestreamer-twitch-gui/releases/download/v${PV}/livestreamer-twitch-gui-v${PV}-linux64.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="-* ~amd64"
IUSE=""

DEPEND=">=net-misc/livestreamer-1.11.1
		>=gnome-base/gconf-3.2.6-r4
		>=x11-libs/libnotify-0.7.6-r3
		>=x11-misc/xdg-utils-1.1.1"
RDEPEND="${DEPEND}"

S="${WORKDIR}/livestreamer-twitch-gui"

src_install() {
	dodir /usr/local/share/livestreamer/twitch-gui
	cp -R "${S}"/* "${D}"/usr/local/share/livestreamer/twitch-gui/ || die "Install failed!"

	dosym /usr/local/share/livestreamer/twitch-gui/livestreamer-twitch-gui /usr/local/bin/twitch-gui
}

pkg_postinst() {
	if host-is-pax; then
		pax-mark -m /usr/local/share/livestreamer/twitch-gui/livestreamer-twitch-gui
	fi
}

