# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"

inherit eutils user linux-info

DESCRIPTION="Open source peer-to-peer VPN"
HOMEPAGE="http://www.peervpn.net/"
MY_P="${PN}-${PV/./-}"
SRC_URI="http://www.peervpn.net/files/${MY_P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="-* ~x86 ~amd64"
IUSE=""

RDEPEND="sys-apps/iproute2"
DEPEND="dev-libs/openssl:0=
		${RDEPEND}"

S="${WORKDIR}"/"${MY_P}"

pkg_setup() {
	# Add peervpn group and user to system
	enewgroup "${PN}"
	enewuser "${PN}" -1 -1 /etc/"${PN}" "${PN}"

	if use kernel_linux; then
		if ! linux_config_exists; then
			eerror "Unable to check your kernel TUN/TAP device driver support"
		else
			CONFIG_CHECK="~TUN"
			ERROR_TUN="You should enable CONFIG_TUN in kernel"
		fi
	fi
}

src_prepare() {
	default

	# Respect CFLAGS
	sed -i -e '/^CFLAGS+=-O2/s///' Makefile || die 'sed on Makefile failed'

	epatch_user
}

src_compile() {
	emake CC=$(tc-getCC) || die 'failed compilation'
}

src_install() {
	dosbin peervpn

	newinitd "${FILESDIR}"/"${PN}".initd "${PN}"

	exeinto /usr/sbin
	newexe "${FILESDIR}"/peervpn-init peervpn-init

	insinto /usr/share/"${PN}"
	doins "${PN}".conf
}

pkg_postinst() {
	ewarn "peervpn needs a valid configuration file to work!"
	elog "An example configuration file has been written to /usr/share/peervpn,"
	elog "copy that to /etc/peervpn/my_vpn.conf and link the init script"
	elog "\t# ln -s /etc/init.d/peervpn /etc/init.d/peervpn.my_vpn"
	elog "\t# rc-service peervpn.my_vpn start"
}

